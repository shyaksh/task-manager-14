package ru.bokhan.tm.api.controller;

public interface IAuthController {

    void login();

    void logout();

    void registry();

}
