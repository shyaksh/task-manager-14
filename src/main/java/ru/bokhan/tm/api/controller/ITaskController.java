package ru.bokhan.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void removeTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void updateTaskById();

    void updateTaskByIndex();

}