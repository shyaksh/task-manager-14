package ru.bokhan.tm.api.controller;

public interface IUserController {

    void updatePassword();

    void updateProfile();

    void showProfile();

}
