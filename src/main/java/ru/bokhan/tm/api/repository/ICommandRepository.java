package ru.bokhan.tm.api.repository;

import ru.bokhan.tm.dto.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}