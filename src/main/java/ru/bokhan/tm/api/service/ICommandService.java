package ru.bokhan.tm.api.service;

import ru.bokhan.tm.dto.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}