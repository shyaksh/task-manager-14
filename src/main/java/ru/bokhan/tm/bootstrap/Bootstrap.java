package ru.bokhan.tm.bootstrap;

import ru.bokhan.tm.api.controller.*;
import ru.bokhan.tm.api.repository.ICommandRepository;
import ru.bokhan.tm.api.repository.IProjectRepository;
import ru.bokhan.tm.api.repository.ITaskRepository;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.api.service.*;
import ru.bokhan.tm.constant.ArgumentConst;
import ru.bokhan.tm.constant.TerminalConst;
import ru.bokhan.tm.controller.*;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.repository.CommandRepository;
import ru.bokhan.tm.repository.ProjectRepository;
import ru.bokhan.tm.repository.TaskRepository;
import ru.bokhan.tm.repository.UserRepository;
import ru.bokhan.tm.service.*;
import ru.bokhan.tm.util.TerminalUtil;

public final class Bootstrap {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final IUserController userController = new UserController(userService, authService);

    private final IAuthController authController = new AuthController(authService);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService, authService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, authService);

    private void initData() {
        final User user = userService.create("test", "test", "test@test.ru");
        final User admin = userService.create("admin", "admin", Role.ADMIN);
        taskService.create(user.getId(), "UserTask1");
        taskService.create(user.getId(), "UserTask2");
        taskService.create(admin.getId(), "AdminTask1");
        taskService.create(admin.getId(), "AdminTask2");
        projectService.create(user.getId(), "UserProject1");
        projectService.create(user.getId(), "UserProject2");
        projectService.create(admin.getId(), "AdminProject1");
        projectService.create(admin.getId(), "AdminProject2");
    }

    public void run(final String[] arguments) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArguments(arguments)) commandController.exit();
        initData();
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    public void parseArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    public void parseCommand(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.LOGIN:
                authController.login();
                break;
            case TerminalConst.LOGOUT:
                authController.logout();
                break;
            case TerminalConst.REGISTRY:
                authController.registry();
                break;
            case TerminalConst.SHOW_PROFILE:
                userController.showProfile();
                break;
            case TerminalConst.UPDATE_PROFILE:
                userController.updateProfile();
                break;
            case TerminalConst.UPDATE_PASSWORD:
                userController.updatePassword();
                break;
        }
    }

    public boolean parseArguments(final String[] arguments) {
        if (arguments == null || arguments.length == 0) return false;
        final String argument = arguments[0];
        parseArgument(argument);
        return true;
    }

}