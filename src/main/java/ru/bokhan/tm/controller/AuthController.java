package ru.bokhan.tm.controller;

import ru.bokhan.tm.api.controller.IAuthController;
import ru.bokhan.tm.api.service.IAuthService;
import ru.bokhan.tm.util.TerminalUtil;

public class AuthController implements IAuthController {

    private final IAuthService authService;

    public AuthController(IAuthService authService) {
        this.authService = authService;
    }

    @Override
    public void login() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        authService.login(login, password);
        System.out.println("[OK:]");
    }

    @Override
    public void logout() {
        System.out.println("[LOGOUT]");
        authService.logout();
        System.out.println("[OK:]");
    }

    @Override
    public void registry() {
        System.out.println("[REGISTRY]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER E-MAIL:]");
        final String email = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        authService.registry(login, password, email);
        System.out.println("[OK:]");
    }

}
