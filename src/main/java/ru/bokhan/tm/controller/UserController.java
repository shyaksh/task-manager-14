package ru.bokhan.tm.controller;

import ru.bokhan.tm.api.controller.IUserController;
import ru.bokhan.tm.api.service.IAuthService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.util.TerminalUtil;

public class UserController implements IUserController {

    private final IUserService userService;

    private final IAuthService authService;

    public UserController(IUserService userService, IAuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    private void showUser(User user) {
        if (user == null) return;
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
    }

    @Override
    public void showProfile() {
        final String userId = authService.getUserId();
        System.out.println("[SHOW PROFILE]");
        final User user = userService.findById(userId);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        showUser(user);
        System.out.println("[OK]");
    }

    @Override
    public void updatePassword() {
        final String userId = authService.getUserId();
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final User user = userService.updatePasswordById(userId, password);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProfile() {
        final String userId = authService.getUserId();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER NEW LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER NEW FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW EMAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = userService.updateById(
                userId, login,
                firstName, lastName, middleName,
                email
        );
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
